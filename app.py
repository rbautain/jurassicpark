from flask import Flask, render_template, abort
import requests
import random
import json
app = Flask(__name__)

#Page principale
@app.route('/')
def homepage():
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    response = response.json()
    return render_template('index.html',response=response)

@app.route('/dinosaur/<dinosaureSlug>')
def dinosaur(dinosaureSlug):
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    response = response.json()
    for dinosaures in response:
        if dinosaures['slug'] == dinosaureSlug:
           UriDinosaure = dinosaures['uri']
           ReponseDinosaure = requests.get(str(UriDinosaure)).json()
           SelectionDinosaure = random.sample(response,3)
           break
    return render_template('dinosaure.html', dinosaure=ReponseDinosaure, SelectionDinosaure=SelectionDinosaure)



